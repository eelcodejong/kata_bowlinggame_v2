package training.catdd;

/**
 * Created by Eelco on 11-May-17.
 */

public class Game {

    private int score;
    private int[] all_rolls = new int[21];
    private int this_roll = 0;

    public void roll(int pins) {
        all_rolls[this_roll] = pins;
        this_roll++;
    }

    public int score() {
        int roll = 0;
        for (int frame = 0; frame < 10; frame++) {
            if (is_spare(roll)) {
                get_spare_and_bonus_score(roll);
                roll = roll + 2;
            } else if (is_strike(roll)) {
                get_strike_and_bonus_score(roll);
                roll = roll + 1;
            } else {
                get_frame_score(roll);
                roll = roll + 2;
            }
        }
        return score;
    }

    private void get_frame_score(int roll) {
        score += all_rolls[roll] + all_rolls[roll + 1];
    }

    private void get_strike_and_bonus_score(int roll) {
        score += 10 + all_rolls[roll + 1] + all_rolls[roll + 2];
    }

    private void get_spare_and_bonus_score(int roll) {
        score += 10 + all_rolls[roll + 2];
    }

    private boolean is_strike(int roll) {
        return all_rolls[roll] == 10;
    }

    private boolean is_spare(int roll) {
        return all_rolls[roll] + all_rolls[roll + 1] == 10;
    }
}
