package training.catdd;

/**
 * Created by Eelco on 11-May-17.
 */

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * TODO: Roll all gutter balls
 * TODO: Roll all ones
 * TODO: Roll one spare
 * TODO: Roll one strike
 * TODO: Roll a perfect game
 */

public class BowlingGame_should {

    private Game game;

    @Before
    public void setUp() throws Exception {
        game = new Game();
    }

    @Test
    public void roll_all_gutter_balls() throws Exception {
        roll_many(20, 0);
        assertThat(game.score(), is(0));
    }

    @Test
    public void roll_all_ones() throws Exception {
        roll_many(20, 1);
        assertThat(game.score(), is(20));
    }

    @Test
    public void roll_one_spare() throws Exception {
        roll_spare();
        game.roll(6);
        assertThat(game.score(), is(22));
    }

    @Test
    public void roll_one_strike() throws Exception {
        roll_strike();
        game.roll(3);
        game.roll(4);
        assertThat(game.score(), is(24));
    }

    @Test
    public void roll_perfect_game() throws Exception {
        roll_many(12, 10);
        assertThat(game.score(), is(300));
    }

    private void roll_strike() {
        game.roll(10);
    }

    private void roll_spare() {
        roll_many(2, 5);
    }

    private void roll_many(int roll, int pins) {
        for (int i = 0; i < roll; i++) {
            game.roll(pins);
        }
    }
}
